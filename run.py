from flask import Flask
from flask_restful import Api
from werkzeug.exceptions import default_exceptions
import os
from app.common.config import config
from app.common import globals
from app.common.utils.imports import import_recursive

if __name__ == '__main__':

    # Setup Flask and Configuration
    flask_app = Flask(__name__)
    flask_api = Api(flask_app)
    app_config = config.Config(flask_app)


    flask_app.config.update(
        PROPAGATE_EXCEPTIONS=True,
        PRESERVE_CONTEXT_ON_EXCEPTION=False,
        DEBUG=app_config.environment.debug
    )

    # Globals
    globals.flask_app = flask_app
    globals.flask_api = flask_api
    globals.config = app_config


    # JWT Init
    from app.common.security import jwt
    jwt.init_jwt(flask_app, app_config)

    # Load API Routes
    path = os.path.join(app_config.paths.project_root, os.path.normpath('app/routes'))
    import_recursive(path)

    # Setup Flask exception handlers, and override default handlers
    import app.common.flask.handlers

    for code, e in default_exceptions.items():
        flask_app.errorhandler(code)(app.common.flask.handlers.http_exception_handler)


    # Start Flask
    flask_app.run(debug=app_config.environment.debug)