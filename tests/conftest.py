import pytest
import requests
import urllib.parse


api_root_url = 'http://localhost:5000'
jwt_header_prefix = 'bearer '


api_users = dict(root=dict(username='admin', password='admin'))


def call_api(endpoint: str = '/', payload: dict=None, token: str=None, method: str='GET'):
    """
    Generic API call
    """
    url = urllib.parse.urljoin(api_root_url, endpoint)

    headers = {}
    if token:
        headers['Authorization'] = '{0}{1}'.format(jwt_header_prefix, token)

    if payload:
        headers['Content-Type'] = 'application/json'

    method = method.upper()

    if method == 'GET':
        response = requests.get(url, headers=headers, json=payload)
    elif method == 'POST':
        response = requests.post(url, headers=headers, json=payload)
    elif method == 'PUT':
        response = requests.put(url, headers=headers, json=payload)
    elif method == 'PATCH':
        response = requests.patch(url, headers=headers, json=payload)
    elif method == 'DELETE':
        response = requests.delete(url, headers=headers, json=payload)
    else:
        # not supported
        assert False, "HTTP method {0} is not supported.".format(method)

    data_out = response.json()
    return data_out



def auth(username, password):
    """
    Calls API auth method and returns token
    """
    payload = dict(username=username, password=password)
    result = call_api("/auth", payload, method='POST')
    return result.get('token', None)




@pytest.fixture(scope="session")
def user_root():
    return api_users['root']


@pytest.fixture(scope="session")
def token_root():
    user = user_root()
    return auth(user['username'], user['password'])
