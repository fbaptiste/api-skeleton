from tests.conftest import call_api


def test_root():
    result = call_api('/', method='GET')
    assert result['api_code'] == 1
    assert result['http_code'] == 200


def test_private_root(token_root):
    result = call_api('/private', token=token_root, method='GET')
    assert result['api_code'] == 1
    assert result['http_code'] == 200

