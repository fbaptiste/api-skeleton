from setuptools import setup

setup(name='api',
      install_requires=[
          'Flask==0.10.1',
          'Flask-RESTful==0.3.5',
          'Flask-Cors==2.1.2',
          'Flask-JWT==0.3.2',
          'configobj==5.0.6',
          'pytest==2.9.1',
          'Sphinx<1.4',
          'requests==2.10.0',
          'passlib==1.6.5',
          'python-dateutil==2.5.3',
          'jsonschema==2.5.1',
          'pytz==2016.4'
      ])
