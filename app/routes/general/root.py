from app.common.globals import flask_api
import app.resources.general.root

flask_api.add_resource(app.resources.general.root.Root, '/', endpoint='public_root')
flask_api.add_resource(app.resources.general.root.PrivateRoot, '/private', endpoint='private_root')
