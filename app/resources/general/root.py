from flask import request
from flask_restful import Resource

from app.common.constants.apicodes import APICodes
from app.common.constants.httpcodes import HTTPCodes
from app.common.flask.decorators import auth, log
from app.common.structures.responses.stdresponse import StdResponse
from app.common.constants.roles import Roles


class Root(Resource):
    @log()
    def get(self):
        result = dict(apiRoot='Public')
        return StdResponse(APICodes.OK, HTTPCodes.OK_200, result, request.url).response


class PrivateRoot(Resource):
    @log()
    @auth(roles_any=[Roles.ROOT])
    def get(self):
        result = dict(apiRoot='Private')
        return StdResponse(APICodes.OK, HTTPCodes.OK_200, result, request.url).response