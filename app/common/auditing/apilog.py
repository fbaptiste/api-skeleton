import platform
from datetime import datetime
from app.common.utils.request import request_json


def apilog(request=None, **kwargs):
    """
    Logs API call
    """

    params = locals().copy()

    try:
        log = dict(dt_utc=datetime.utcnow())

        for key, value in params.items():
            if value is not None and key != 'kwargs' and key != 'request':
                log[key] = value

        for key, value in kwargs.items():
            if value is not None:
                log[key] = value

        if 'request' in params:
            log['http_method'] = request.method
            log['endpoint'] = request.endpoint
            log['url'] = request.url
            log['path'] = request.path
            log['remote_addr'] = request.remote_addr
            log['user_agent'] = request.user_agent.string

            log['request_values'] = dict()
            if len(request.values) > 0:
                log['request_values']['form_arg'] = request.values.to_dict()

            try:
                doc = request_json(request, True)
                if doc:
                    log['request_values']['json'] = doc
            except:
                pass

            if 'HTTP_AUTHORIZATION' in request.headers.environ:
                auth_header = request.headers.environ['HTTP_AUTHORIZATION']
                log['auth_header'] = auth_header
                if 'bearer' in auth_header:
                    token = auth_header.replace("bearer", "").strip()
                    log['jwt_token'] = token

        try:
            host = platform.node()
            log['host'] = host
        except:
            host = None


        try:
            # TODO: Persist log object to database / file
            print(log)
        except:
            pass

    except:
        pass

