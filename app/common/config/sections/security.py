

class Security:
    def __init__(self, section):
        self.jwt_key = section.get('jwt_key', None)
        self.jwt_expiration = section.get('jwt_exp', 86400)
        self.pwd_hashing_num_passes = section.get('pwd_hashing_num_passes', 2000)
        self.pwd_hashing_salt_size = section.get('pwd_hashing_salt_size', 16)
