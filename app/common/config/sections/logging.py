import logging
import os
from logging.handlers import RotatingFileHandler
from logging.handlers import SMTPHandler
from flask import Flask

from app.common.config.sections.email import Email
from app.common.config.sections.paths import Paths


class Logging:
    def __init__(self, section, *, app_name: str, flask_app: Flask, email: Email, paths: Paths):
        self.console_level = section.get('console_level', logging.INFO) if section else logging.INFO
        self.file_level = section.get('file_level', logging.INFO) if section else logging.INFO
        self.email_level = section.get('email_level', logging.CRITICAL) if section else logging.CRITICAL
        self.log_dir = section.get('log_dir', None) if section else None
        self.enable_log_to_email = section.get('enable_log_to_email', False) if section else False
        self.enable_log_to_file = section.get('enable_log_to_file', False) if section else False

        self.configure_logging(app_name=app_name, flask_app=flask_app, email=email, paths=paths)


    def configure_logging(self, *, app_name: str, flask_app: Flask, email: Email, paths: Paths):
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                            datefmt='%m/%d/%Y %H:%M:%S',
                            level=logging.getLevelName(self.console_level))

        if self.enable_log_to_email:
            if email.username or email.password:
                credentials = (email.username, email.password)
            else:
                credentials = None

            mail_handler = SMTPHandler((email.host, email.port),
                                       'no-reply@' + email.host,
                                       email.admins,
                                       "{0} Error".format(app_name),
                                       credentials)

            mail_handler.setFormatter(logging.Formatter(fmt='%(asctime)s : %(levelname)s : %(message)s [in %(pathname)s:%(lineno)d]',
                                      datefmt='%m/%d/%Y %H:%M:%S'))

            mail_handler.setLevel(self.email_level)
            flask_app.logger.addHandler(mail_handler)

        if self.enable_log_to_file:
            log_file_path = os.path.join(paths.project_root, self.log_dir, '{0}.log'.format(app_name))
            file_handler = RotatingFileHandler(log_file_path, 'a', 1 * 1024 * 1024, 10)
            file_handler.setFormatter(logging.Formatter(fmt='%(asctime)s : %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]',
                                      datefmt='%m/%d/%Y %H:%M:%S'))
            file_handler.setLevel(self.file_level)
            flask_app.logger.addHandler(file_handler)


        flask_app.logger.info('log level (default) = %s' % logging.getLevelName(self.console_level))
        flask_app.logger.info('log level (file) = %s' % logging.getLevelName(self.file_level))
        flask_app.logger.info('log level (email) = %s' % logging.getLevelName(self.email_level))