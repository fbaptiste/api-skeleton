import os


class Paths:
    def __init__(self, section):
        self.json_schema_path = section.get('json_schema_path', None) if section else None
        self.project_root = os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir, os.pardir))
