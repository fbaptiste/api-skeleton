

class Email:
    def __init__(self, section):
        self.host = section.get('host', None)
        self.port = section.get('port', None)
        self.username = section.get('username', None)
        self.password = section.get('password', None)

        self.admins = section['admins']
