import os
import sys
from configobj import ConfigObj, ConfigObjError, flatten_errors
from flask import Flask
from validate import Validator

from app.common.config.sections.email import Email
from app.common.config.sections.environment import Environment
from app.common.config.sections.logging import Logging
from app.common.config.sections.paths import Paths
from app.common.config.sections.security import Security


class Config:
    def __init__(self, flask_app: Flask):
        self.flask_app = flask_app
        self.app_name = 'api'

        main_config_fn = 'main.ini'
        configspec_fn = 'configspec.ini'

        local_path = os.path.join(os.path.dirname(__file__))
        app_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir))
        config_path = os.path.join(app_path, 'config')
        main_config_path = os.path.join(config_path, main_config_fn)
        configspec_path = os.path.join(local_path, configspec_fn)

        # Load main config file
        try:
            cfg_main = ConfigObj(main_config_path, configspec=configspec_path, file_error=True)
        except(ConfigObjError, IOError) as e:
            print('Could not read {0}: {1}'.format(main_config_path, e))
            sys.exit(1)

        # See if a secondary config file was specified
        section_env = cfg_main.get('Environment', None)
        if section_env:
            env = section_env.get('env', None)
        else:
            env = None

        if env:
            env_config_path = os.path.join(config_path, '{0}.ini'.format(env))
            try:
                cfg_env = ConfigObj(env_config_path, file_error=True)
            except(ConfigObjError, IOError) as e:
                print('Could not read {0}: {1}'.format(main_config_path, e))
                sys.exit(1)
            else:
                cfg_main.merge(cfg_env)

        # Validate config file
        validator = Validator()
        results = cfg_main.validate(validator)

        if results is not True:
            for (section_list, key, _) in flatten_errors(cfg_main, results):
                if key is not None:
                    print('Key ({0}) in section [{1}] failed validation'.format(key, ', '.join(section_list)))
                else:
                    print('Section {0} was missing.'.format(', '.join(section_list)))
            sys.exit(1)

        # Load sections
        section_environment = 'Environment'
        section_security = 'Security'
        section_paths = 'Paths'
        section_email = 'Email'
        section_logging = 'Logging'

        self.environment = Environment(cfg_main.get(section_environment, None))
        self.security = Security(cfg_main.get(section_security, None))
        self.paths = Paths(cfg_main.get(section_paths, None))
        self.email = Email(cfg_main.get(section_email, None))
        self.logging = Logging(cfg_main.get(section_logging, None),
                               app_name=self.app_name,
                               flask_app=self.flask_app,
                               email=self.email,
                               paths=self.paths)



