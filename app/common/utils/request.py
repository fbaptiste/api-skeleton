from app.common.structures.responses.stderror import StdError
from app.common.constants.apicodes import APICodes
from app.common.constants.httpcodes import HTTPCodes


def request_json(request, silent=False):
    try:
        doc = request.get_json()
        return doc
    except:
        if silent:
            return None
        else:
            raise StdError(APICodes.PAYLOAD_INVALID, HTTPCodes.BAD_REQUEST_400, "Missing or invalid JSON.")