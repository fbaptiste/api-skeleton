import datetime
import json
from time import mktime
import calendar
import decimal


class JSONEncoder(json.JSONEncoder):
    """
    JSON encoder
    - Serializes dates as plain strings
    """
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return str(o)

        if isinstance(o, datetime.date):
            return str(o)

        if isinstance(o, decimal.Decimal):
            return float(o)

        return json.JSONEncoder.default(self, o)



def encode(doc):
    results = json.loads(json.dumps(doc, cls=JSONEncoder))
    return results
