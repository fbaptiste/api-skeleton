from app.common.config.config import Config
from flask import Flask
from flask_restful import Api

config = None  # type: Config
flask_app = None  # type: Flask
flask_api = None  # type: Api
