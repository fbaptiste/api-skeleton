from flask import jsonify

from app.common.constants.httpcodes import HTTPCodes
from app.common.structures.responses.responsecode import ResponseCode


class StdError(Exception):
    """
    Standard Error Response
    """
    def __init__(self, api_response: ResponseCode, http_response: ResponseCode, details=None, errors=None, request_url=None):
        self.__api_response = api_response
        if http_response:
            self.__http_response = http_response
        else:
            self.__http_response = HTTPCodes.BAD_REQUEST_400
        self.__details = details
        self.__errors = errors
        self.__request_url = request_url

    def __str__(self):
        return "{0} - {1}".format(self.__api_response.code, self.__api_response.message)

    def to_dict(self):
        rv = dict(http_message=self.__http_response.message,
                  http_code=self.__http_response.code,
                  api_message=self.__api_response.message,
                  api_code=self.__api_response.code)

        if self.__details:
            rv['details'] = self.__details

        if self.__errors:
            rv['errors'] = self.__errors

        if self.__request_url:
            rv['href'] = self.__request_url

        return rv

    @property
    def request_url(self):
        return self.__request_url

    @request_url.setter
    def request_url(self, value):
        self.__request_url = value

    @property
    def response(self):
        rv = self.to_dict()
        return jsonify(rv), self.__http_response.code

    @property
    def details(self):
        if self.__details:
            return self.__details
        else:
            return self.__api_response.message

    @property
    def api_response(self):
        return self.__api_response
