import platform
from flask import jsonify, make_response

from app.common.constants.httpcodes import HTTPCodes
from app.common.structures.responses.responsecode import ResponseCode




class StdResponse:
    """
    Standard API response
    """
    def __init__(self, api_response: ResponseCode, http_response: ResponseCode, results=None, request_url=None):
        """
        @type api_response: ResponseCode
        @type http_response: ResponseCode
        @type results: dict
        @type request_url: str
        """
        self.results = results
        self.request_url = request_url
        self.api_response = api_response

        if http_response:
            self.http_response = http_response
        else:
            self.http_response = HTTPCodes.OK_200

        try:
            self.host = platform.node()
        except:
            self.host = None


    def __str__(self):
        return "API: {0} - {1}, HTTP: {2}".format(self.api_response.code, self.api_response.message, self.http_response.code)

    def to_dict(self):
        result = dict(http_message=self.http_response.message,
                      http_code=self.http_response.code,
                      api_message=self.api_response.message,
                      api_code=self.api_response.code,
                      host=self.host)

        if self.results:
            result['results'] = self.results

        if self.request_url:
            result['href'] = self.request_url

        return result

    @property
    def response(self):
        rv = self.to_dict()
        return make_response(jsonify(rv), self.http_response.code)
