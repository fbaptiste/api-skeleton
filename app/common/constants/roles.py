from enum import Enum


class Roles(Enum):
    PUBLIC = 'public'
    ROOT = 'root'
