from app.common.structures.responses.responsecode import ResponseCode


class APICodes:
    # Generic error
    GENERAL_ERROR = ResponseCode(0, 'General error.')



    # OK Results: 1 - 999
    OK = ResponseCode(1, 'OK.')



    # Server Level Errors: 1000 - 5000
    SCHEMA_NOT_FOUND = ResponseCode(1500, 'Expected JSON schema file was not found on server.')
    SCHEMA_INVALID = ResponseCode(1501, 'The server JSON schema file is invalid.')



    # User Level Errors: 5000+
    RESOURCE_NOT_FOUND = ResponseCode(5000, 'Specified resource was not found.')

    FORBIDDEN_NOT_AUTHORIZED = ResponseCode(5100, 'Auth token required.')
    FORBIDDEN_INSUFFICIENT_ROLES = ResponseCode(5101, 'Forbidden: User role insufficient.')

    PAYLOAD_MISSING = ResponseCode(5200, 'A JSON payload was expected.')
    PAYLOAD_INVALID = ResponseCode(5201, 'JSON payload is invalid.')

    METHOD_NOT_ALLOWED = ResponseCode(5300, "Method is not allowed.")

