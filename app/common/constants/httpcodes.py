from app.common.structures.responses.responsecode import ResponseCode


class HTTPCodes:
    OK_200 = ResponseCode(200, "OK")
    CREATED_201 = ResponseCode(201, "Created.")
    ACCEPTED_202 = ResponseCode(202, "Accepted.")
    NO_CONTENT_204 = ResponseCode(204, "No Content.")

    BAD_REQUEST_400 = ResponseCode(400, "Bad Request.")
    UNAUTHORIZED_401 = ResponseCode(401, "Unauthorized.")
    FORBIDDEN_403 = ResponseCode(403, "Forbidden.")
    NOT_FOUND_404 = ResponseCode(404, "Not Found.")
    METHOD_NOT_ALLOWED_405 = ResponseCode(405, "Method not allowed.")
    CONFLICT_409 = ResponseCode(409, "Conflict.")

    INTERNAL_SERVER_500 = ResponseCode(500, "Internal Server Error")
    UNAVAILABLE_503 = ResponseCode(503, "Service Unavailable.")
