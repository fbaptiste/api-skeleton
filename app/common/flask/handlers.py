from flask import request

from app.common.globals import flask_app
from app.common.constants.apicodes import APICodes
from app.common.constants.httpcodes import HTTPCodes
from app.common.structures.responses.stderror import StdError
from app.common.structures.responses.responsecode import ResponseCode



def http_exception_handler(error):
    if error and hasattr(error, 'code'):
        http_code = error.code

        if hasattr(error, 'description'):
            http_message = error.description
        else:
            http_message = 'Unknown HTTP error.'

        http_response = ResponseCode(http_code, http_message)

        if http_response.code == HTTPCodes.NOT_FOUND_404.code:
            api_response = APICodes.RESOURCE_NOT_FOUND
        elif http_response.code == HTTPCodes.METHOD_NOT_ALLOWED_405.code:
            api_response = APICodes.METHOD_NOT_ALLOWED
        else:
            api_response = APICodes.GENERAL_ERROR

        request_method = request.method if request and hasattr(request, 'method') else 'N/A'
        request_path = request.path if request and hasattr(request, 'path') else 'N/A'
        details = dict(method=request_method, path=request_path, error=str(error))
        flask_app.logger.error(details)
        api_error = StdError(api_response, http_response, details)
        return api_error.response

    else:
        raise error.get_response()



@flask_app.errorhandler(StdError)
def api_exception(error: StdError):
    error.request_url = request.url if request and hasattr(request, 'url') else ''
    request_method = request.method if request and hasattr(request, 'method') else 'N/A'
    request_path = request.path if request and hasattr(request, 'path') else 'N/A'
    log_details = dict(method=request_method, path=request_path, details=error.details)
    flask_app.logger.error(log_details)
    return error.response


@flask_app.errorhandler(Exception)
def generic_exception(error):
    request_method = request.method if request and hasattr(request, 'method') else 'N/A'
    request_path = request.path if request and hasattr(request, 'path') else 'N/A'
    details = dict(method=request_method, path=request_path, error=str(error))
    flask_app.logger.error(details)
    api_error = StdError(APICodes.GENERAL_ERROR, HTTPCodes.INTERNAL_SERVER_500, details)
    return api_error.response
