from functools import wraps

import flask_jwt
from flask import request
from flask_jwt import current_identity

from app.common.globals import flask_app
from app.common.constants.apicodes import APICodes
from app.common.constants.roles import Roles
from app.common.constants.httpcodes import HTTPCodes
from app.common.auditing.apilog import apilog
from app.common.structures.responses.stderror import StdError


def auth(roles_any=list(), roles_every=list(), realm=None):
    """
    Decorator used to validate call against JWT user and roles
    """
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            # validate JWT token
            try:
                flask_jwt._jwt_required(realm or flask_app.config['JWT_DEFAULT_REALM'])

            except flask_jwt.JWTError as e:
                raise StdError(APICodes.FORBIDDEN_NOT_AUTHORIZED,
                               HTTPCodes.UNAUTHORIZED_401,
                               details="{0}: {1}".format(e.error, e.description))

            # validate roles (at least one in the roles_any list, and all roles in roles_every list)
            is_ok = True
            user_roles = current_identity['roles']

            # Check to make sure each specified role in [any] and [all] is an APIRole object, not just a string
            for r in roles_any:
                if not(r in Roles):
                    details = "Unrecognized user roles."
                    raise StdError(APICodes.FORBIDDEN_INSUFFICIENT_ROLES, HTTPCodes.FORBIDDEN_403, details, None, request.url)

            for r in roles_every:
                if not(r in Roles):
                    details = "Unrecognized user roles."
                    raise StdError(APICodes.FORBIDDEN_INSUFFICIENT_ROLES, HTTPCodes.FORBIDDEN_403, details, None, request.url)


            for role in roles_every:
                if role.value not in user_roles:
                    is_ok = False

            if is_ok and len(roles_any) > 0:
                is_ok = False
                for role in roles_any:
                    if role.value in user_roles:
                        is_ok = True
                        break

            if not is_ok:
                details = "Insufficient user roles."
                raise StdError(APICodes.FORBIDDEN_INSUFFICIENT_ROLES, HTTPCodes.FORBIDDEN_403, details, None, request.url)

            return fn(*args, **kwargs)
        return decorator
    return wrapper



def log(**ext_kwargs):
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **inner_kwargs):
            try:
                user_name = current_identity.get('username', None)
            except:
                user_name = None

            apilog(user=user_name, request=request, **ext_kwargs)
            return fn(*args, **inner_kwargs)
        return decorator
    return wrapper
