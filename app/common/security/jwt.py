from datetime import datetime, timedelta

from flask import jsonify

from app.common.constants.apicodes import APICodes
from app.common.constants.httpcodes import HTTPCodes
from app.common.globals import flask_app
from app.common.security.authentication import authenticate_user, lookup_user
from app.common.structures.responses.stderror import StdError
from app.common.utils import json


def init_jwt(fl_app, config):
    from flask_jwt import JWT

    fl_app.config['SECRET_KEY'] = config.security.jwt_key
    fl_app.config['JWT_EXPIRATION_DELTA'] = timedelta(seconds=config.security.jwt_expiration)
    fl_app.config['JWT_REQUIRED_CLAIMS'] = ['username', 'iat', 'exp', 'nbf']
    fl_app.config['JWT_AUTH_HEADER_PREFIX'] = "Bearer"
    fl_app.jwt = JWT(flask_app, authentication_handler, identity_handler)
    fl_app.jwt.jwt_payload_callback = payload_handler
    fl_app.jwt.jwt_error_callback = error_handler
    fl_app.jwt.auth_response_callback = auth_resp_handler



def auth_resp_handler(access_token, id):
    """
    JWT Auth response handler - returns token JSON from JWT's access_token
    """
    return jsonify({'token': access_token.decode('utf-8')})



def authentication_handler(username, password):
    """
    JWT Auth handler
    Authenticates user credentials and returns User object
    If authenticated OK, continues with payload_handler
    """

    user = authenticate_user(username, password)
    if user is not None:
        return user



def payload_handler(user):
    """
    JWT Payload callback
    This call creates the payload for the specified user
    """

    iat = datetime.utcnow()
    exp = iat + flask_app.config.get('JWT_EXPIRATION_DELTA')
    nbf = iat + flask_app.config.get('JWT_NOT_BEFORE_DELTA')

    result = dict(username=user['username'],
                  iat=iat,
                  exp=exp,
                  nbf=nbf)

    return result


def identity_handler(payload):
    """
    JWT Identity Handler
    Takes the token payload and returns the corresponding user
    """
    if payload is not None:
        username = payload['username']
        iat = payload['iat']
        exp = payload['exp']
        nbf = payload['nbf']
        user = lookup_user(username=username)
        if user is not None:
            user['iat'] = iat
            user['exp'] = exp
            user['nbf'] = nbf

            result = json.encode(user)
            return result


def error_handler(e):
    """
    JWT Error handler
    """
    try:
        description = e.description
    except:
        description = str(e)

    return StdError(APICodes.FORBIDDEN_NOT_AUTHORIZED, HTTPCodes.UNAUTHORIZED_401, details=description).response
